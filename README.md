# pyDigest - Roman Law in machine-readable format

The _Digest_ is the definitive Roman law compendium compiled under emperor Justinian I (533 CE). The project applies computational methods to the _Digest_ text to get deep insights about Roman law which would supplement and guide close reading research.

The research is carried out at the University of Surrey School of Law as part of an Early Career Research Fellowship funded by The Leverhulme Trust (ECF-2019-418). The project's current public [GitLab repository](https://gitlab.eps.surrey.ac.uk/mr0048/pydigest) includes scripts and files accompanied by detailed documentation.

<img src="/images/Leverhulme_Trust_RGB_blue.png"  height="120"> <img src="/images/Surrey_SoL_logo.jpg"  height="160"> <img src="/images/gitlab-logo-gray-rgb.png"  height="120">

## Project components

[0. "pyDigest" - General functions](/pyDigest_documentation.md)

[1. "Ddf" - Core _Digest_ dataframes](/Ddf_documentation.md)

[2. "NLP" - Natural Language Processing](/NLP_documentation.md)

[3. "SQL" - Relational database](/SQL_documentation.md)

[4. "Stats" - Statistical analysis and data visualisation](/Stats_documentation.md)