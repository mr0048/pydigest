# Import packages and the benchmark file
from gensim.models.fasttext import load_facebook_model
import pandas as pd
import re
benchmark_path = '/home/mribary/Dropbox/pyDigest/dump/wordvec/syn-selection-benchmark-Latin-legal.tsv' 
benchmark = pd.read_csv(benchmark_path, sep='\t', header=None)

###########
#| Lasla |#
###########

# Load the pre-trained Lasla fasttext model
lasla_path = '/home/mribary/Dropbox/pyDigest/dump/wordvec/lasla_skip.bin'
lasla_model = load_facebook_model(lasla_path)

# Create dataframe with top 10 semantic neighbours for words in benchmark
top10dict = {'word':[], 'lasla_neighbours':[], 'lasla_scores':[]}
model = lasla_model
for i in range(len(benchmark)):
    source = benchmark.iloc[i][0]
    words = model.wv.most_similar(source)
    top10dict['word'].append(source)
    neighbours = []
    scores = []
    for j in range(len(words)):
        neighbour = words[j][0].lower().replace('_', '').replace('-', '')
        neighbour = re.sub('\d+', '', neighbour)
        score = words[j][1]
        neighbours.append(neighbour)
        scores.append(score)
    top10dict['lasla_neighbours'].append(neighbours)
    top10dict['lasla_scores'].append(scores)
top10_lasla = pd.DataFrame(top10dict)
top10_lasla.to_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_lasla.pkl')
top10_lasla = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_lasla.pkl')
print('Lasla top10 dataframe complete')

##############
#| LatinISE |#
##############

# Load the pre-trained fasttext model
latinise_path = '/home/mribary/Dropbox/pyDigest/dump/wordvec/latinise_skip.bin'
latinise_model = load_facebook_model(latinise_path)

# Create dataframe with top 10 semantic neighbours for words in benchmark
top10dict = {'word':[], 'latinise_neighbours':[], 'latinise_scores':[]}
model = latinise_model
for i in range(len(benchmark)):
    source = benchmark.iloc[i][0]
    words = model.wv.most_similar(source)
    top10dict['word'].append(source)
    neighbours = []
    scores = []
    for j in range(len(words)):
        neighbour = words[j][0].lower().replace('_', '').replace('-', '')
        neighbour = re.sub('\d+', '', neighbour)
        score = words[j][1]
        neighbours.append(neighbour)
        scores.append(score)
    top10dict['latinise_neighbours'].append(neighbours)
    top10dict['latinise_scores'].append(scores)
top10_latinise = pd.DataFrame(top10dict)
top10_latinise.to_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_latinise.pkl')
top10_latinise = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_latinise.pkl')
print('Latinise top10 dataframe complete')

#############
#| ROMTEXT |#
#############

# Load the pre-trained ROMTEXT fasttext model
romtext_path = '/home/mribary/Dropbox/pyDigest/dump/wordvec/romtext_skip.bin'
romtext_model = load_facebook_model(romtext_path)

# Create dataframe with top 10 semantic neighbours for words in benchmark
top10dict = {'word':[], 'romtext_neighbours':[], 'romtext_scores':[]}
model = romtext_model
for i in range(len(benchmark)):
    source = benchmark.iloc[i][0]
    words = model.wv.most_similar(source)
    top10dict['word'].append(source)
    neighbours = []
    scores = []
    for j in range(len(words)):
        neighbour = words[j][0].lower().replace('_', '').replace('-', '')
        neighbour = re.sub('\d+', '', neighbour)
        score = words[j][1]
        neighbours.append(neighbour)
        scores.append(score)
    top10dict['romtext_neighbours'].append(neighbours)
    top10dict['romtext_scores'].append(scores)
top10_romtext = pd.DataFrame(top10dict)
top10_romtext.to_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_romtext.pkl')
top10_romtext = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_romtext.pkl')
print('ROMTEXT top10 dataframe complete')


############
#| Digest |#
############

# Load the pre-trained Digest fasttext model
digest_path = '/home/mribary/Dropbox/pyDigest/dump/wordvec/digest_skip.bin'
digest_model = load_facebook_model(digest_path)

# Create dataframe with top 10 semantic neighbours for words in benchmark
top10dict = {'word':[], 'digest_neighbours':[], 'digest_scores':[]}
model = digest_model
for i in range(len(benchmark)):
    source = benchmark.iloc[i][0]
    words = model.wv.most_similar(source)
    top10dict['word'].append(source)
    neighbours = []
    scores = []
    for j in range(len(words)):
        neighbour = words[j][0].lower().replace('_', '').replace('-', '')
        neighbour = re.sub('\d+', '', neighbour)
        score = words[j][1]
        neighbours.append(neighbour)
        scores.append(score)
    top10dict['digest_neighbours'].append(neighbours)
    top10dict['digest_scores'].append(scores)
top10_digest = pd.DataFrame(top10dict)
top10_digest.to_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_digest.pkl')
top10_digest = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_digest.pkl')
print('Digest top10 dataframe complete')

# Load the dataframes from pickle files
top10_lasla = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_lasla.pkl')
top10_latinise = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_latinise.pkl')
top10_romtext = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_romtext.pkl')
top10_digest = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10_digest.pkl')

# Create ab aggregate dataframe
top10 = top10_lasla
top10['latinise_neighbours'] = top10_latinise['latinise_neighbours']
top10['latinise_scores'] = top10_latinise['latinise_scores']
top10['romtext_neighbours'] = top10_romtext['romtext_neighbours']
top10['romtext_scores'] = top10_romtext['romtext_scores']
top10['digest_neighbours'] = top10_digest['digest_neighbours']
top10['digest_scores'] = top10_digest['digest_scores']
top10.to_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10.pkl')
top10 = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10.pkl')