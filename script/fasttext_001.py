# Import packages
import pandas as pd
import numpy as np
import re
import fasttext
from pyDigest import latin_lemma_text
# The script requires cltk installed

# Load stoplist
path_stoplist = 'dump/D_stoplist_001.txt'
digest_stoplist = list(pd.read_csv(path_stoplist, header=None)[0])  # 57 custom stopwords

#############
#| ROMTEXT |#
#############

# Load the ROMTEXT corpus from a txt file
romtext_path = 'dump/wordvec/romtext.txt'
with open(romtext_path) as content:
    romtext = content.read()

# Remove notes stored between "<>"
romtext = re.sub('<.*>', '', romtext)
units = romtext.split('\n\n')
# print(len(units)) # 41196

# Keep text lines only (include lower case characters)
lines = []
for i in range(len(units)):
    units[i] = re.sub('.*\n', '', units[i])
    if re.search('[a-z]', units[i]):
        lines.append(units[i])
del lines[0] # Remove copyright notice
# print(len(lines)) # 39368

# Create text files for fasttext - lemma text units in new lines in one continuous string
romtext_lemma_text = latin_lemma_text(lines)
romtext_lemma_path = 'dump/wordvec/romtext_lemma.txt'
with open(romtext_lemma_path, "w") as f:
    text = '\n'.join([str(textunit) for textunit in romtext_lemma_text])
    print(text, file=f)

# Train and save skipgram model :
romtext_skip = fasttext.train_unsupervised(romtext_lemma_path, model='skipgram')
romtext_skip.save_model('dump/wordvec/romtext_skip.bin')

############
#| Digest |#
############

# Load dataframes
file_path_df = 'dump/Ddf_v106.csv'
df = pd.read_csv(file_path_df, index_col=0)     # text units (21055)
digest_list_of_texts = list(df.TextUnit)

# Create text files for fasttext - text units in new lines in one continuous string
digest_lemma_text_stop = latin_lemma_text(digest_list_of_texts)
digest_lemma_path = 'dump/wordvec/dtext_stop.txt'
with open(digest_lemma_path, "w") as f:
    text = '\n'.join([str(textunit) for textunit in digest_lemma_text_stop])        
    print(text, file=f)

# Train and save Digest skipgram model
digest_skip = fasttext.train_unsupervised(digest_lemma_path, model='skipgram')
digest_skip.save_model('dump/wordvec/digest_skip.bin')

###########
#| Lasla |#
###########

# Load trained models of LiLa embeddings based on the Lasla corpus

# Lasla skipgram model
path_lasla_skip = 'dump/wordvec/lasla_skip.bin'
lasla_skip = fasttext.load_model(path_lasla_skip)

##############
#| LatinISE |#
##############

# Load the LatinISE corpus from a txt file
latinise_path = 'dump/wordvec/latin13.txt'
with open(latinise_path) as content:
    latinise = content.read()

# Split the corpus on the <doc> tag
docs = latinise.split('</doc>')
# print(len(docs)) # 1273 documents in the corpus

# Select docs from the three "Romana" eras
romana_docs = []
eras = []
for i in range(len(docs)):
    if re.search('(?:<doc).+', docs[i]) is not None:
        head = re.search('(?:<doc).+', docs[i]).group()
        if re.search('era="[A-Za-z\s]+"', head) is not None:
            era = re.search('era="[A-Za-z\s]+"', head).group()
            eras.append(era)
            if 'Romana' in era:
                romana_docs.append(docs[i])
    else:
        # print(i)
        del docs[i]
# print(set(eras))
# print(len(romana_docs)) # 903 documents from one of the three "Romana" eras

# Create corpus of lemma sentences
corpus = []
for i in range(len(romana_docs)):
    sentences = romana_docs[i].split('.\tPUN\t.\n')
    for j in range(len(sentences)):
        sentences[j] = re.sub("<.+\n?","",sentences[j])
        lemmas = re.findall('(?:\n[A-Za-z]+\t[A-Z]+\t)([A-Za-z]+)', sentences[j])
        lemma_sent = ' '.join(lemmas)
        corpus.append(lemma_sent)
# print(len(corpus)) # 348053 sentences in the corpus

# Create text file for fasttext - lemma sentences in new lines in one continuous string
latinise_lemma_path = 'dump/wordvec/latinise_lemma.txt'
with open(latinise_lemma_path, "w") as f:
    text = '\n'.join([str(sentence) for sentence in corpus])
    print(text, file=f)

# Train and save LatinISE skipgram model
latinise_skip = fasttext.train_unsupervised(latinise_lemma_path, model='skipgram')
latinise_skip.save_model('dump/wordvec/latinise_skip.bin')

################
#| Evaluation |#
################

# Import the eval function and gensim's load_facebook_model function
from pyDigest import eval
from gensim.models.fasttext import load_facebook_model

# Load the pre-trained fasttext model
lasla_path = 'dump/wordvec/lasla_skip.bin'
lasla_model = load_facebook_model(lasla_path)
print(eval(lasla_model)) # 85.57% matches LiLa's synonyms

# Load the pre-trained fasttext model
latinise_path = 'dump/wordvec/latinise_skip.bin'
latinise_model = load_facebook_model(latinise_path)
print(eval(latinise_model)) # 87.86% matches LiLa's synonyms

# Load the pre-trained fasttext model
romtext_path = 'dump/wordvec/romtext_skip.bin'
romtext_model = load_facebook_model(romtext_path)
print(eval(romtext_model)) # 67.15% matches LiLa's synonyms

# Load the pre-trained fasttext model
digest_path = 'dump/wordvec/digest_skip.bin'
digest_model = load_facebook_model(digest_path)
print(eval(digest_model)) # 62.80% matches LiLa's synonyms