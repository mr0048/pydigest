# Import numpy and pandas
import pandas as pd
import numpy as np

# Load BKO_v004.csv into a pandas data frame and strip whitespace
BKOdf = pd.read_csv('./dump/BKO_v004.csv')
for col in ['Work_ref', 'Honore_group_name', 'Honore_group_type', 'Jurist_name', \
    'Work_title', 'BK_mass', 'Note']:
    BKOdf[col] = BKOdf[col].str.strip()
BKOdf['bib_ID'] = BKOdf.index
# print(BKOdf.head(10))
# BKOdf.info()

# Load the ID and Jurists dataframes from csv files
Ddf_IDs = pd.read_csv('./dump/Ddf_IDs_000.csv', index_col=0)
# Ddf_IDs.info()

Jurists = pd.read_csv('./dump/Jurists_v002.csv', index_col=0)
# print(DdfIDs.head(10))
# print(Jurists.head(10))

# Load text
texts = pd.read_csv('./dump/Ddf_v106.csv', index_col=0)

# Merge all dataframes
df1 = pd.merge(BKOdf, Jurists, how='left', left_on='Jurist_name', right_on='Jurist')
# df1.info()
df2 = pd.merge(Ddf_IDs, df1, how='left', on='bib_ID')
# df2.info()
df3 = pd.merge(df2, texts, how='left', left_index=True, right_index=True)
# df3.info()

Ddf_IDs = df3[['BKO_id', 'Jurist_id', 'Jurist', 'Mid_date', 'BKO_key', 'Work_id', \
    'Work_title', 'Book_id', 'TextUnit_ref', 'bib_ID']]
Ddf_IDs.info()
# print(Ddf_IDs.head(10))

# Export ID dataframe
# BKO.to_csv("./dump/BKO_v005.csv")
Ddf_IDs.to_csv("./dump/Ddf_IDs_v003.csv")