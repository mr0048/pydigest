################
#| Evaluation |#
################

def eval(model, type='general'):
    '''
    The function takes a Latin gensim-FastText object and prints the TOEFL-style
    synonym evaluation based on LiLa's benchmark (`general`) and the adapted benchmark
    based on Berger's Dictionary of Roman law (`legal`).
    '''
    # Load benchmark TOEFL synonyms
    import pandas as pd
    benchmark_path = 'https://gitlab.eps.surrey.ac.uk/mr0048/pydigest/-/raw/master/dump/wordvec/syn-selection-benchmark-Latin-legal.tsv' 
    benchmark = pd.read_csv(benchmark_path, sep='\t', header=None)

    from math import isnan

    if type == 'general':
        true = 0
        total = 0
        for j in range(len(benchmark)):
            check = model.wv.most_similar(benchmark.iloc[j][0])
            if isnan(check[0][1]):
                next
            else:
                total += 1
                scores = []
                for i in range(2, 6):
                    source = benchmark.iloc[j][0]
                    target = benchmark.iloc[j][i]
                    score = model.wv.similarity(w1=source, w2=target)
                    scores.append(score)
                if scores[0] == max(scores):
                    true += 1
        print('Result of evaluation based on the general benchmark:')
        print('Number of term(s) missing from the model and removed from evaluation: ' + str(len(benchmark) - total))
        print(str(round((true/total)*100, 2)) + "% matches LiLa's semantic neighbours")

    if type == 'legal':
        true = 0
        total = 0
        for j in range(len(benchmark)):
            if pd.isna(benchmark.iloc[j][1]):
                next
            else:
                total += 1
                scores = []
                for i in [1, 3, 4, 5]:
                    source = benchmark.iloc[j][0]
                    target = benchmark.iloc[j][i]
                    score = model.wv.similarity(w1=source, w2=target)
                    scores.append(score)
                if scores[0] == max(scores):
                    true += 1
        print('Result of evaluation based on the legal benchmark:')
        print(str(round((true/total)*100, 2)) + "% matches Berger's semantic neighbours")

# Import gensim's load_facebook_model function
from gensim.models.fasttext import load_facebook_model

# Load the pre-trained fasttext model
lasla_path = 'dump/wordvec/lasla_skip.bin'
lasla_model = load_facebook_model(lasla_path)
print(eval(lasla_model, type='general')) # 85.56% matches LiLa's synonyms
print(eval(lasla_model, type='legal')) # 64.69% matches Berger's neighbours

# Load the pre-trained fasttext model
latinise_path = 'dump/wordvec/latinise_skip.bin'
latinise_model = load_facebook_model(latinise_path)
print(eval(latinise_model, type='general')) # 87.80% matches LiLa's synonyms
print(eval(latinise_model, type='legal')) # 70.19% matches Berger's neighbours

# Load the pre-trained fasttext model
romtext_path = 'dump/wordvec/romtext_skip.bin'
romtext_model = load_facebook_model(romtext_path)
print(eval(romtext_model, type='general')) # 67.44% matches LiLa's synonyms
print(eval(romtext_model, type='legal')) # 61.31% matches Berger's neighbours

# Load the pre-trained fasttext model
digest_path = 'dump/wordvec/digest_skip.bin'
digest_model = load_facebook_model(digest_path)
print(eval(digest_model, type='general')) # 62.87% matches LiLa's synonyms
print(eval(digest_model, type='legal')) # 51.37% matches Berger's neighbours