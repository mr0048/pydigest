# Import packages
import pandas as pd
import numpy as np

# Load Ddf.csv into a pandas data frame and strip whitespace
load_columns = ['BKO_key', 'Work', 'TextUnit_ref']
df = pd.read_csv('./dump/Ddf_v105.csv', usecols=load_columns)
df.BKO_key = df.BKO_key.str.strip()
df.Work = df.Work.str.strip()
df.TextUnit_ref = df.TextUnit_ref.str.strip()
# print(df.head(10))

# Load BKO_v004.csv into a pandas data frame and strip whitespace
load_columns = ['Work_ref']
BKOdf = pd.read_csv('./dump/BKO_v004.csv', usecols=load_columns)
BKOdf.Work_ref = BKOdf.Work_ref.str.strip()
# print(BKOdf.head(10))

#######################################
# Create dataframes for reference IDs #
#######################################

# Unique BKO_keys in df
BKO_keys = pd.DataFrame(sorted(df.BKO_key.unique()))
BKO_keys[1] = range(len(BKO_keys))
BKO_keys.rename(columns={0: 'BKO_label', 1: 'BKO_id'}, inplace=True)
BKO_keys['BKO_label'] = BKO_keys['BKO_label'].astype('category')
BKO_keys = BKO_keys[['BKO_id', 'BKO_label']]
# print(BKO_keys.head())
# print(len(BKO_keys)) # 293

# Unique works in df
Works = pd.DataFrame(sorted(df.Work.unique()))
Works[1] = range(len(Works))
Works.rename(columns={0: 'Work_label', 1: 'Work_id'}, inplace=True)
Works['Work_label'] = Works['Work_label'].astype('category')
Works = Works[['Work_id', 'Work_label']]
# print(Works.head())
# print(len(Works)) # 250

# Unique libri in df
Books = pd.DataFrame(sorted(df.TextUnit_ref.unique()))
Books[1] = range(len(Books))
Books.rename(columns={0: 'Book_label', 1: 'Book_id'}, inplace=True)
Books['Book_label'] = Books['Book_label'].astype('category')
Books = Books[['Book_id', 'Book_label']]
# print(Books.head())
# print(len(Books)) # 1381

###################################################
# Link reference IDs to the indices of text units #
###################################################

# Attach IDs to BKOdf
BKOdfID = pd.merge(BKOdf, BKO_keys, how='left', left_on='Work_ref', right_on='BKO_label')
BKOdfID['bib_ID'] = BKOdfID.index

# Attach IDs to df
ID1 = pd.merge(df, BKO_keys, how='left', left_on='BKO_key', right_on='BKO_label')
ID1.drop(columns=['BKO_label'], inplace=True)
ID2 = pd.merge(ID1, Works, how='left', left_on='Work', right_on='Work_label')
ID2.drop(columns=['Work_label'], inplace=True)
ID3 = pd.merge(ID2, Books, how='left', left_on='TextUnit_ref', right_on='Book_label')
ID3.drop(columns=['Book_label'], inplace=True)
ID4 = pd.merge(ID3, BKOdfID, how='left', on='BKO_id')
ID4.drop(columns=['Work_ref', 'BKO_label'], inplace=True)
# print(ID4.head(10))

# Strcuture ID4 for output
for col in ['BKO_key', 'Work', 'TextUnit_ref']:
    ID4[col] = ID4[col].astype('category')
# print(ID4.head(10))
# ID4.info() # all str values as category, al IDs as int64

# Export ID dataframe: Ddf_IDs_000.csv
ID4.to_csv("./dump/Ddf_IDs_000.csv")