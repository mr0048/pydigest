# Import packages
import pandas as pd
import numpy as np
import re
import fasttext
from pyDigest import latin_lemma_text
# The script requires cltk installed

#############
#| ROMTEXT |#
#############

# Load the ROMTEXT corpus from a txt file
romtext_path = 'dump/wordvec/romtext.txt'
with open(romtext_path) as content:
    romtext = content.read()

# Remove notes stored between "<>"
romtext = re.sub('<.*>', '', romtext)
units = romtext.split('\n\n')
# print(len(units)) # 41196

# Keep text lines only (include lower case characters)
lines = []
for i in range(len(units)):
    units[i] = re.sub('.*\n', '', units[i])
    if re.search('[a-z]', units[i]):
        lines.append(units[i])
del lines[0] # Remove copyright notice
print('Number of lines in ROMTEXT: ' + str(len(lines))) # 39368

# fasttext format lemma test
romtext_lemma_text = latin_lemma_text(lines)

# Create list of lemmas with 3 or less occurences in ROMTEXT
romtext_one_string = ' '.join([str(textunit) for textunit in romtext_lemma_text])
romtext_tokens = romtext_one_string.split()
romtext_series = pd.Series(romtext_tokens)
romtext_wordcount = romtext_series.value_counts().reset_index()
romtext_wordcount.rename(columns={'index': 'lemma', 0: 'count'}, inplace = True)
romtext_lowfreq = list(romtext_wordcount.lemma[romtext_wordcount['count'] < 3].values)
print('Number of low frequency words in ROMTEXT: ' + str(len(romtext_lowfreq)))

# Remove low frequency lemmas
total = len(romtext_lemma_text)
count = 0
new_romtext_lemma_text = []
for i in range(len(romtext_lemma_text)):
    words = romtext_lemma_text[i].split()
    new_words = []
    for j in range(len(words)):
        if words[j] not in romtext_lowfreq:
            new_words.append(words[j])
        new_line = ' '.join(new_words)
    new_romtext_lemma_text.append(new_line)
    count += 1
    print('ROMTEXT: ' + str("%.2f" % round(((count / total)*100), 2)) + '%', end='\r')

# Count words before and after
total_words = 0
new_total_words = 0
for i in range(len(romtext_lemma_text)):
    words = romtext_lemma_text[i].split()
    new_words = new_romtext_lemma_text[i].split()
    total_words += len(words)
    new_total_words += len(new_words)
print('Total words before removing low frequency words: ' + str(total_words))
print('Total words after removing low frequency words: ' + str(new_total_words))
print('Difference: ' + str(total_words - new_total_words))

# Create text files for fasttext - lemma text units in new lines in one continuous string
romtext_lemma_path = 'dump/wordvec/romtext_lemma.txt'
with open(romtext_lemma_path, "w") as f:
    text = '\n'.join([str(textunit) for textunit in new_romtext_lemma_text])
    print(text, file=f)

# Train and save skipgram model :
romtext_skip = fasttext.train_unsupervised(romtext_lemma_path, model='skipgram')
romtext_skip.save_model('dump/wordvec/romtext_skip.bin')

############
#| Digest |#
############

# Load dataframes
file_path_df = 'dump/Ddf_v106.csv'
df = pd.read_csv(file_path_df, index_col=0)     # text units (21055)
digest_list_of_texts = list(df.TextUnit)

# Create text files for fasttext - text units in new lines in one continuous string
digest_lemma_text = latin_lemma_text(digest_list_of_texts)

# Remove low frequency ROMTEXT lemmas
total = len(digest_lemma_text)
count = 0
new_digest_lemma_text = []
for i in range(len(digest_lemma_text)):
    words = digest_lemma_text[i].split()
    new_words = []
    for j in range(len(words)):
        if words[j] not in romtext_lowfreq:
            new_words.append(words[j])
        new_line = ' '.join(new_words)
    new_digest_lemma_text.append(new_line)
    count += 1
    print('Digest: ' + str("%.2f" % round(((count / total)*100), 2)) + '%', end='\r')

# Count words before and after
total_words = 0
new_total_words = 0
for i in range(len(digest_lemma_text)):
    words = digest_lemma_text[i].split()
    new_words = new_digest_lemma_text[i].split()
    total_words += len(words)
    new_total_words += len(new_words)
print('Total words before removing low frequency words: ' + str(total_words))
print('Total words after removing low frequency words: ' + str(new_total_words))
print('Difference: ' + str(total_words - new_total_words))

# Create text files for fasttext - lemma text units in new lines in one continuous string
digest_lemma_path = 'dump/wordvec/digest_lemma.txt'
with open(digest_lemma_path, "w") as f:
    text = '\n'.join([str(textunit) for textunit in new_digest_lemma_text])        
    print(text, file=f)

# Train and save Digest skipgram model
digest_skip = fasttext.train_unsupervised(digest_lemma_path, model='skipgram')
digest_skip.save_model('dump/wordvec/digest_skip.bin')

###########
#| Lasla |#
###########

# Load trained models of LiLa embeddings based on the Lasla corpus

# Lasla skipgram model
# path_lasla_skip = 'dump/wordvec/lasla_skip.bin'
# lasla_skip = fasttext.load_model(path_lasla_skip)

##############
#| LatinISE |#
##############

# Load the LatinISE corpus from a txt file
latinise_path = 'dump/wordvec/latin13.txt'
with open(latinise_path) as content:
    latinise = content.read()

# Split the corpus on the <doc> tag
docs = latinise.split('</doc>')
print('LatinISE corpus:')
print('Number of documents in the corpus: ' + str(len(docs))) # 1273 documents in the corpus

# Select docs from the three "Romana" eras
romana_docs = []
eras = []
for i in range(len(docs)):
    if re.search('(?:<doc).+', docs[i]) is not None:
        head = re.search('(?:<doc).+', docs[i]).group()
        if re.search('era="[A-Za-z\s]+"', head) is not None:
            era = re.search('era="[A-Za-z\s]+"', head).group()
            eras.append(era)
            if 'Romana' in era:
                romana_docs.append(docs[i])
    else:
        # print(i)
        del docs[i]
# print(set(eras))
print('Number of documents in the Romana eras: ' + str(len(romana_docs)))
    # 903 documents from one of the three "Romana" eras

# Create corpus of lemma sentences
corpus = []
for i in range(len(romana_docs)):
    sentences = romana_docs[i].split('.\tPUN\t.\n')
    for j in range(len(sentences)):
        sentences[j] = re.sub("<.+\n?","",sentences[j])
        lemmas = re.findall('(?:\n[A-Za-z]+\t[A-Z]+\t)([A-Za-z]+)', sentences[j])
        lemma_sent = ' '.join(lemmas)
        corpus.append(lemma_sent)
print('Number of sentences: ' + str(len(corpus))) # 348053 sentences in the corpus

# Run the Burns lemmatizer on `corpus`
latinise_lemma_text = latin_lemma_text(corpus)

# Create text file for fasttext - lemma sentences in new lines in one continuous string
latinise_lemma_path = 'dump/wordvec/latinise_lemma.txt'
with open(latinise_lemma_path, "w") as f:
    text = '\n'.join([str(sentence) for sentence in latinise_lemma_text])
    print(text, file=f)

# Train and save LatinISE skipgram model
latinise_skip = fasttext.train_unsupervised(latinise_lemma_path, model='skipgram')
latinise_skip.save_model('dump/wordvec/latinise_skip.bin')