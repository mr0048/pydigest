# Import packages, load dataframes
import pandas as pd
benchmark_path = '/home/mribary/Dropbox/pyDigest/dump/wordvec/syn-selection-benchmark-Latin-legal.tsv' 
benchmark = pd.read_csv(benchmark_path, sep='\t', header=None)
top10 = pd.read_pickle('/home/mribary/Dropbox/pyDigest/dump/wordvec/top10.pkl')

#######################
# Pairwise dataframes #
#######################

# Dataframe #1 - Full

words = []
las_lat = []
las_rom = []
las_dig = []
lat_rom = []
lat_dig = []
dig_rom = []
for i in range(len(top10)):
    word = top10.iloc[i,0]
    set_las = set(top10.iloc[i,1])
    set_lat = set(top10.iloc[i,3])
    set_rom = set(top10.iloc[i,5])
    set_dig = set(top10.iloc[i,7])
    overlap_las_lat = 10 - len(set_las - set_lat)
    overlap_las_rom = 10 - len(set_las - set_rom)
    overlap_las_dig = 10 - len(set_las - set_dig)
    overlap_lat_rom = 10 - len(set_lat - set_rom)
    overlap_lat_dig = 10 - len(set_lat - set_dig)
    overlap_dig_rom = 10 - len(set_dig - set_rom)
    words.append(word)
    las_lat.append(overlap_las_lat)
    las_rom.append(overlap_las_rom)
    las_dig.append(overlap_las_dig)
    lat_rom.append(overlap_lat_rom)
    lat_dig.append(overlap_lat_dig)
    dig_rom.append(overlap_dig_rom)
pairwise_full = pd.DataFrame(list(zip(words, las_lat, las_rom, las_dig, lat_rom, lat_dig, dig_rom)), columns = ['word', 'las_lat', 'las_rom', 'las_dig', 'lat_rom', 'lat_dig', 'rom_dig'])
pairwise_full.to_csv('/home/mribary/Dropbox/pyDigest/dump/wordvec/pairwise_full.csv')
print('Full dataframe saved as "pairwise_full.csv":')
print('Number of rows: ' + str(len(pairwise_full)))

# Dataframe #2 - Minimum

# Create a mask to filter rows including at least one 0 overlap
mask_min = []
for i in range(len(pairwise_full)):
    row_mask = []
    for j in range(1,7):
        if pairwise_full.iloc[i,j] == 0:
            row_mask.append(False)
        else:
            row_mask.append(True)
    if False in row_mask:
        mask_min.append(False)
    else:
        mask_min.append(True)
pairwise_min = pairwise_full[mask_min]
pairwise_min.to_csv('/home/mribary/Dropbox/pyDigest/dump/wordvec/pairwise_min.csv')
print('Minimum dataframe saved as "pairwise_min.csv":')
print(str(len(pairwise_full) - sum(mask_min)) + ' rows including at least one 0 overlap have been dropped')

# Dataframe #3 - Maximum

# Create a mask to filter rows with all 0 overlap
mask_max = []
for i in range(len(pairwise_full)):
    row_mask = []
    for j in range(1,7):
        if pairwise_full.iloc[i,j] == 0:
            row_mask.append(False)
        else:
            row_mask.append(True)
    if True in row_mask:
        mask_max.append(True)
    else:
        mask_max.append(False)
pairwise_max = pairwise_full[mask_max]
pairwise_max.to_csv('/home/mribary/Dropbox/pyDigest/dump/wordvec/pairwise_max.csv')
print('Minimum dataframe saved as "pairwise_max.csv":')
print(str(len(pairwise_full) - len(pairwise_max)) + ' rows including 0s only have been dropped')

# Dataframe #4 - Weighted

# Create list of tuples for word-score combinations
las_lat = (1, 2, 3, 4)
las_rom = (1, 2, 5, 6)
las_dig = (1, 2, 7, 8)
lat_rom = (3, 4, 5, 6)
lat_dig = (3, 4, 7, 8)
rom_dig = (5, 6, 7, 8)
pairs = [las_lat, las_rom, las_dig, lat_rom, lat_dig, rom_dig]

# Calculate weighted pairwise overlap of words between models
rows = []
for l in range(len(top10)):
    row = top10.iloc[l]
    scores = [top10.iloc[l][0]]
    for k in range(len(pairs)):
        pair = pairs[k]
        first_word = row[pair[0]]
        first_score = row[pair[1]]
        second_word = row[pair[2]]
        second_score = row[pair[3]]
        score = 0
        for i in range(10):
            for j in range(10):
                if first_word[i] == second_word[j]:
                    score += first_score[i] * second_score[j]
                    #print(first_word[i], first_score[i], second_word[j], second_score[j])
                    #print(first_score[i] * second_score[j])
        scores.append(score)
    rows.append(scores)

# Create dataframe with weighted overlap scores
pairwise_weighted = pd.DataFrame(rows)
pairwise_weighted.columns = ['word', 'las_lat', 'las_rom', 'las_dig', 'lat_rom', 'lat_dig', 'rom_dig']
pairwise_weighted.to_csv('/home/mribary/Dropbox/pyDigest/dump/wordvec/pairwise_weighted.csv')
print('Weighted overlap dataframe saved as "pairwise_weighted.csv":')
print('Number of rows: ' + str(len(pairwise_weighted)))

#############
# Analytics #
#############

#1 Heatmap

# Define function to generate heatmap

def heatmap(df):
    """
    Create a Grey-toned seaborn heatmap to visualise
    correlation between fastText models.
    df: dataframe with pairwise overlap scores
    """
    import seaborn as sns
    a = df.las_lat.mean()
    b = df.las_rom.mean()
    c = df.las_dig.mean()
    d = df.lat_rom.mean()
    e = df.lat_dig.mean()
    f = df.rom_dig.mean()
    means = [a, b, c, d, e, f]
    df_heat = pd.DataFrame(zip([10, a, b, c], [a, 10, d, e], [b, d, 10, f], [c, e, f, 10]))
    df_heat.index = ['las', 'lat', 'rom', 'dig']
    df_heat.columns = ['las', 'lat', 'rom', 'dig']
    df_heat.mask(df_heat == 10, inplace=True)
    return sns.heatmap(df_heat, vmin=min(means), vmax=max(means), annot=True, cmap='Greys')

# Create and save heatmaps

# 1. Full
pairwise_full_heatmap = heatmap(pairwise_full).get_figure()
#pairwise_full_heatmap.savefig('/home/mribary/Dropbox/pyDigest/images/pairwise_full_heatmap.png')

# 2. Minimum
pairwise_min_heatmap = heatmap(pairwise_min).get_figure()
#pairwise_min_heatmap.savefig('/home/mribary/Dropbox/pyDigest/images/pairwise_min_heatmap.png')

# 3. Maximum
pairwise_max_heatmap = heatmap(pairwise_max).get_figure()
#pairwise_max_heatmap.savefig('/home/mribary/Dropbox/pyDigest/images/pairwise_max_heatmap.png')

# 4. Weighted
pairwise_weighted_heatmap = heatmap(pairwise_weighted).get_figure()
#pairwise_weighted_heatmap.savefig('/home/mribary/Dropbox/pyDigest/images/pairwise_weighted_heatmap.png')

#2 Sorted LatinISE-ROMTEXT dataframes

def lat_rom_sort(df):
    return df[['word', 'lat_rom']].sort_values(by=['lat_rom'], ascending=False)

lat_rom_full = lat_rom_sort(pairwise_full)
lat_rom_full.to_csv('/home/mribary/Dropbox/pyDigest/dump/wordvec/lat_rom_full.csv')

lat_rom_min = lat_rom_sort(pairwise_min)
lat_rom_min.to_csv('/home/mribary/Dropbox/pyDigest/dump/wordvec/lat_rom_min.csv')

lat_rom_max = lat_rom_sort(pairwise_max)
lat_rom_max.to_csv('/home/mribary/Dropbox/pyDigest/dump/wordvec/lat_rom_max.csv')

lat_rom_weighted = lat_rom_sort(pairwise_weighted)
lat_rom_weighted.to_csv('/home/mribary/Dropbox/pyDigest/dump/wordvec/lat_rom_weighted.csv')